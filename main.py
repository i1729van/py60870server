import asyncio
import time
import logging
from client.master import Client104
from server.slave import Server104
from server.xml import ServerConfigFromXml

logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d:%H:%M:%S',
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)


async def run_client():
    client = Client104()
    client.start()
    logger.debug("Hello From Client Task")
    await asyncio.sleep(4)

    client.stop()


async def run_server():
    cfg = ServerConfigFromXml('config.xml').data
    server = Server104(cfg)
    server.start()
    logger.debug("Hello From Server Task")
    await asyncio.sleep(9)


async def main():
    logger.setLevel(logging.DEBUG)
    task_server = asyncio.create_task(run_server())
    task_client = asyncio.create_task(run_client())
    logger.debug(f"started at {time.strftime('%X')}")
    await task_server
    await task_client

    logger.debug(f"finished at {time.strftime('%X')}")

asyncio.run(main())
